# zundscope

zund usb microscope tool with wifi streaming

<img src='img/zundscope.jpg' height=400px><br>



<img src='img/UDT_corner-2.jpg' height=300px>
<img src='img/UDT_corner.jpg' height=300px>


## Connecting to the scope

1. Add power through USB battery (remember to push the button)
2. Give Linux about ~60 seconds to start up.
3. Join the "Zundtastic" wifi network from any nearby computer. 
4. Open a terminal and log into the tool: `ssh pi@192.168.4.1`
5. Start the webcam server: `/usr/local/bin/mjpg_streamer -i "/usr/local/lib/mjpg-streamer/input_uvc.so -n -f 15 -r 1920x1080" -o "/usr/local/lib/mjpg-streamer/output_http.so -p 8080 -w /usr/local/www"`
6. Open a web browser and go to `http://192.168.4.1:8080/target.html` to view the stream.
7. When you are ready to shut down the scope, from the terminal run `sudo shutdown now`
8. Unplug the battery.

## BOM

1. <a href='https://www.adafruit.com/product/3400'>Raspberry Pi Zero W</a>, $10.00
2. <a href='https://www.amazon.com/Supereyes-Portable-Microscope-Endoscope-Magnifier/dp/B0066H7H1Q'>SuperEyes B008 USB Microscope</a>, $95.50
3. <a href='https://www.amazon.com/Anker-PowerCore-Lipstick-Sized-Generation-Batteries/dp/B005X1Y7I2'>Anker 3350mAh USB Battery</a>, $19.99
4. Flat tipped set screw 5mmx0.8mmx10mm, McMaster 92605A233, $6.38 for pack of 50 (only need 8)
5. Low-profile socket head cap screws 3mm (TODO, find exact link)
6. USB type A plug (TODO, find exact link)

For alignment, I find this 2.5mm hex-shank allen bit helpful: McMaster 7389A22

Also, for tapping the M3 mounting holes for the raspberry pi, a tap is helpful (can also just use the screws to threadform).  E.G. McMaster 8302A12

## Building the scope


### 3D models to print

The main body, with supports for 3D printing is available <a href='model/zundscope_with_supports.stl'>here</a>.

<img src='img/slicer.jpg' height=400px>


### Setting up Raspberry Pi

TODO: host OS image for preconfigured download for flashing SD cards

TODO: make detailed installation instructions from vanilla raspian

### Assembly

TODO

<img src='img/assembly.jpg' height=400px>

The brightness of the LEDs around the camera is set by a resistor value.  Unfortunately the circuit used by this SuperEyes microscope is quite sensitive to this value (higher resisistance is brighter, so it's like the the LEDs and resistor are in parallel, rather than series as is usual with current limiting resistors... not sure why).  I've found 330Kohm to be a good value for this.


### Alignment

The tool can be aligned to the rotational axis of the universal module by adjusting the set screws on the lower part of the tool body.  This allows the microscope to be used for accurate registration tasks (in addition to simple measurement and inspection tasks, for which alignment isn't necessary).

TODO: write detailed instructions.


## Future tasks

1. Make a startup service that starts the webcam server.  Need to issue the shutdown command via the webpage to avoid potential corruption of SD card.
2. Would be great to adjust field of view without disassembling the scope.  Maybe as simple as an access hole for adjusting knob.
3. Figure out how to use for Registration, not just Reference in ZCC.
